package com.echarity.android;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentActivity extends AppCompatActivity {

    @BindView(R.id.input_name)
    EditText _nameText;
    @BindView(R.id.input_email) EditText _emailText;
    @BindView(R.id.input_mobile) EditText _mobileText;
    @BindView(R.id.input_amount) EditText amountText;
    @BindView(R.id.btn_donate)
    Button btnDonate;
    @BindView(R.id.spinnerZone)
    Spinner spinnerZone;
    @BindView(R.id.spinnerUnit) Spinner spinnerUnit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        ButterKnife.bind(this);

        btnDonate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                donateMoney();
            }
        });

        List<String> zones = new ArrayList<String>();
        zones.add("Zone");
        zones.add("Zone 1");
        zones.add("Zone 2");
        zones.add("Zone 3");
        zones.add("Zone 4");
        zones.add("Zone 5");

        List<String> units = new ArrayList<String>();
        units.add("Unit");
        units.add("Unit 1");
        units.add("Unit 2");
        units.add("Unit 3");
        units.add("Unit 4");
        units.add("Unit 5");

        ArrayAdapter<String> dataAdapterZone = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, zones);
        dataAdapterZone.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerZone.setAdapter(dataAdapterZone);

        ArrayAdapter<String> dataAdapterUnt = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, units);
        dataAdapterUnt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerUnit.setAdapter(dataAdapterUnt);
    }

    public void donateMoney(){
        if (!validate()) {
            onDonateFailed();
            return;
        }
        btnDonate.setEnabled(false);
        final ProgressDialog progressDialog = new ProgressDialog(PaymentActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Payment in progress...");
        progressDialog.show();

        // TODO: Implement your own signup logic here.

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onSignupSuccess or onSignupFailed
                        // depending on success
                        onDonateSuccess();
                        // onSignupFailed();
                        progressDialog.dismiss();
                    }
                }, 3000);
    }

    public void onDonateSuccess() {
        btnDonate.setEnabled(true);
        setResult(RESULT_OK, null);
        finish();
    }

    public void onDonateFailed() {
        Toast.makeText(getBaseContext(), "Donation failed", Toast.LENGTH_SHORT).show();
        btnDonate.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String name = _nameText.getText().toString();
        String email = _emailText.getText().toString();
        String mobile = _mobileText.getText().toString();
        String amount = amountText.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            _nameText.requestFocus();
            _nameText.setError("at least 3 characters", null);
            valid = false;
            return valid;
        } else {
            _nameText.setError(null, null);
        }


        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.requestFocus();
            _emailText.setError("enter a valid email address", null);
            valid = false;
            return valid;
        } else {
            _emailText.setError(null, null);
        }

        if (mobile.isEmpty() || mobile.length()!=10) {
            _mobileText.requestFocus();
            _mobileText.setError("Enter Valid Mobile Number", null);
            valid = false;
            return valid;
        } else {
            _mobileText.setError(null, null);
        }

        int payingAmount = 0;
        if(amount != null && amount.length() > 0){
            payingAmount = Integer.parseInt(amount);
        }
        if (payingAmount <= 0) {
            amountText.requestFocus();
            amountText.setError("Enter Valid Amount", null);
            valid = false;
            return valid;
        } else {
            amountText.setError(null, null);
        }

        return valid;
    }
}
