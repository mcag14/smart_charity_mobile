package com.echarity.android;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignUpOptionsActivity extends AppCompatActivity {

    private static final int REQUEST_SIGNUP_SINGLE_OPTION = 201;
    private static final int REQUEST_SIGNUP_MONTHLY_OPTION = 202;

    @BindView(R.id.btnSinglePayment) Button btnSinglePayment;

    @BindView(R.id.btnMonthlyPayment) Button btnMonthlyPayment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_options);
        ButterKnife.bind(this);

        btnSinglePayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), PaymentActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP_SINGLE_OPTION);
            }
        });

        btnMonthlyPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP_MONTHLY_OPTION);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_SIGNUP_MONTHLY_OPTION) {
                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically
                this.finish();
            }
            else if(requestCode == REQUEST_SIGNUP_SINGLE_OPTION){

            }
        }
    }
}
