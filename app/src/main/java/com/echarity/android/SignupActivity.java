package com.echarity.android;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SignupActivity extends AppCompatActivity {

    private static final String TAG = "SignupActivity";


    @BindView(R.id.input_name) EditText _nameText;
    @BindView(R.id.input_address) EditText _addressText;
    @BindView(R.id.input_district) EditText districtText;
    @BindView(R.id.input_email) EditText _emailText;
    @BindView(R.id.input_mobile) EditText _mobileText;
    @BindView(R.id.input_password) EditText _passwordText;
    @BindView(R.id.input_reEnterPassword) EditText _reEnterPasswordText;
    @BindView(R.id.btn_signup) Button _signupButton;
    @BindView(R.id.link_login) TextView _loginLink;
    @BindView(R.id.spinnerZone) Spinner spinnerZone;
    @BindView(R.id.spinnerUnit) Spinner spinnerUnit;
    @BindView(R.id.checkboxAccept)
    CheckBox checkboxAccept;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

        _loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish the registration screen and return to the Login activity
                Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        List<String> zones = new ArrayList<String>();
        zones.add("Zone");
        zones.add("Zone 1");
        zones.add("Zone 2");
        zones.add("Zone 3");
        zones.add("Zone 4");
        zones.add("Zone 5");

        List<String> units = new ArrayList<String>();
        units.add("Unit");
        units.add("Unit 1");
        units.add("Unit 2");
        units.add("Unit 3");
        units.add("Unit 4");
        units.add("Unit 5");

        ArrayAdapter<String> dataAdapterZone = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, zones);
        dataAdapterZone.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerZone.setAdapter(dataAdapterZone);

        ArrayAdapter<String> dataAdapterUnt = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, units);
        dataAdapterUnt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerUnit.setAdapter(dataAdapterUnt);
    }

    public void signup() {
        Log.d(TAG, "Signup");

        if (!validate()) {
            onSignupFailed();
            return;
        }

        _signupButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(SignupActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating Account...");
        progressDialog.show();

        String name = _nameText.getText().toString();
        String address = _addressText.getText().toString();
        String district = districtText.getText().toString();
        String email = _emailText.getText().toString();
        String mobile = _mobileText.getText().toString();
        String password = _passwordText.getText().toString();
        String reEnterPassword = _reEnterPasswordText.getText().toString();

        // TODO: Implement your own signup logic here.

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onSignupSuccess or onSignupFailed
                        // depending on success
                        onSignupSuccess();
                        // onSignupFailed();
                        progressDialog.dismiss();
                    }
                }, 3000);
    }


    public void onSignupSuccess() {
        _signupButton.setEnabled(true);
        setResult(RESULT_OK, null);
        finish();
    }

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_SHORT).show();

        _signupButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String name = _nameText.getText().toString();
        String address = _addressText.getText().toString();
        String email = _emailText.getText().toString();
        String mobile = _mobileText.getText().toString();
        String password = _passwordText.getText().toString();
        String reEnterPassword = _reEnterPasswordText.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            _nameText.requestFocus();
            _nameText.setError("at least 3 characters", null);
            valid = false;
            return valid;
        } else {
            _nameText.setError(null, null);
        }

        if (address.isEmpty()) {
            _addressText.requestFocus();
            _addressText.setError("Enter Valid Address", null);
            valid = false;
            return valid;
        } else {
            _addressText.setError(null, null);
        }

        if (address.isEmpty()) {
            districtText.requestFocus();
            districtText.setError("Enter Valid District", null);
            valid = false;
            return valid;
        } else {
            districtText.setError(null, null);
        }


        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.requestFocus();
            _emailText.setError("enter a valid email address", null);
            valid = false;
            return valid;
        } else {
            _emailText.setError(null, null);
        }

        if (mobile.isEmpty() || mobile.length()!=10) {
            _mobileText.requestFocus();
            _mobileText.setError("Enter Valid Mobile Number", null);
            valid = false;
            return valid;
        } else {
            _mobileText.setError(null, null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.requestFocus();
            _passwordText.setError("between 4 and 10 alphanumeric characters", null);
            valid = false;
            return valid;
        } else {
            _passwordText.setError(null, null);
        }

        if (reEnterPassword.isEmpty() || reEnterPassword.length() < 4 || reEnterPassword.length() > 10 || !(reEnterPassword.equals(password))) {
            _reEnterPasswordText.requestFocus();
            _reEnterPasswordText.setError("Password Do not match", null);
            valid = false;
            return valid;
        } else {
            _reEnterPasswordText.setError(null, null);
        }

        if(!checkboxAccept.isChecked()){
            valid = false;
            Toast.makeText(this, "Please accept terms and conditions", Toast.LENGTH_SHORT).show();
        }

        return valid;
    }
}
