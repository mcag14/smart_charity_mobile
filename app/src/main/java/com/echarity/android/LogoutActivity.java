package com.echarity.android;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import android.content.Intent;

public class LogoutActivity extends AppCompatActivity {
    @BindView(R.id.btn_more)
    Button btnMore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout);
        ButterKnife.bind(this);
        btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                btnMore.setEnabled(false);
                final ProgressDialog progressDialog = new ProgressDialog(LogoutActivity.this,
                        R.style.AppTheme_Dark_Dialog);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Logout...");
                progressDialog.show();

                // TODO: Implement your own logout logic here.
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                // On complete call either onSignupSuccess or onSignupFailed
                                // depending on success
                                //onDonateSuccess();
                                // onSignupFailed();
                                progressDialog.dismiss();
                            }
                        }, 10000);
            }


        });

    }

}
